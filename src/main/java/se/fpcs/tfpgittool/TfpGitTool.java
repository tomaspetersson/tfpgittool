package se.fpcs.tfpgittool;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TfpGitTool {

    public void execute(String[] args) throws IOException, GitAPIException {

        if (args == null || args.length == 0) {
            System.err.println(String.format("Usage: %s <base-folder>", getClass().getName()));
            System.exit(1);
        }

        File baseFolder = new File(args[0]);
        if (!baseFolder.exists()) {
            System.err.println(String.format("Error: base folder not found: %s", baseFolder.getAbsolutePath()));
            System.exit(2);
        }

        List<File> gitRepoList = getGitRepoList(baseFolder);
        for (File gitRepo : gitRepoList) {
            getGitStatus(gitRepo);
        }

    }

    private void getGitStatus(File gitRepo) throws IOException, GitAPIException {

        //System.out.println(String.format("Checking status on: %s", gitRepo));
        Git git = Git.open(gitRepo);
        Status status = git.status().call();
        if(status.hasUncommittedChanges()) {
            System.out.println(String.format("Has uncommitted changes: %s", gitRepo));
            for(String uncommittedChange: status.getUncommittedChanges()){
                System.out.println("\tUncommitted change: " + uncommittedChange);
            }
        }

        if(!status.getUntracked().isEmpty()) {
            System.out.println(String.format("Has untracked files: %s", gitRepo));
            for(String untracked: status.getUntracked()){
                System.out.println("\tUntracked file: " + untracked);
            }
        }

    }

    /**
     * @param baseFolder
     * @return
     */
    private List<File> getGitRepoList(File baseFolder) {

        List<File> gitRepoList = new ArrayList<File>();
        for (File folder : baseFolder.listFiles()) {
            if (!folder.isDirectory()) {
                continue;
            }
            File dotGitFolder = new File(folder, ".git");
            if (dotGitFolder.exists()) {
                gitRepoList.add(folder);
                //System.out.println("Added Git repo: " + folder.getAbsolutePath());
            }
        }
        return gitRepoList;

    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        new TfpGitTool().execute(args);
    }

}
